import {Route, Switch} from "react-router-dom";
import React, {Component} from 'react'
import Calendar from './Calendar'

class App extends Component {
    render() {
        return (
            <Switch>
                <Route path="/:dayId?" component={Calendar} />
            </Switch>
        );
    };
}

export default App