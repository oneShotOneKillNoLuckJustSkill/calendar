import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Badge} from 'reactstrap'
import './cell.css'


class Cell extends Component {
    onClick = (e) => {
        this.props.onDayClick(e, this.props.date)
    }

    render() {
        const {date, isWeekDay = true, events = [], isCurrent = false} = this.props
        return (
            <div
                onClick={this.onClick}
                className={'cellSize'}
                style={{
                    outline: '1px solid black',
                    backgroundColor: isWeekDay ? 'transparent' : 'grey',
                }}>
                <div style={{
                    display: 'flex',
                    justifyContent: 'flex-end',
                    paddingRight: '5%',
                }}>
                    <Badge color={isCurrent ? 'danger' : null}>{date}</Badge>


                </div>
                {events.map((item, index) => (
                    <span
                        style={{
                            fontSize: '10px'
                        }}
                        key={index}>
                            {item}
                    </span>))}
            </div>
        )
    }

}

Cell.propTypes = {
    date: PropTypes.number.isRequired,
    isWeekDay: PropTypes.bool,
    events: PropTypes.array,
    isCurrent: PropTypes.bool,
}

export default Cell