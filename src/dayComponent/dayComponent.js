import React, {Component} from 'react'
import {Button, Modal, ModalHeader, ModalBody, ModalFooter} from 'reactstrap';
import {Rnd} from 'react-rnd'
import moment from 'moment'


class DayComponent extends Component {

    state = {
        itemArray: [],
    }

    createTimeRange = (e) => {
        if(moment(this.props.dayId)._d < new Date()){
            return
        }
        const item = this.state.itemArray;
        item.push(
            <Rnd
                bounds={'parent'}
                minWidth={'calc(100% - 30px - 5%)'}
                height={200}
                style={{
                    backgroundColor: 'red',
                }}
            >
                <span>Event</span>
            </Rnd>
        )
        this.setState({itemArray: item})
    }

    render() {
        const {events = [], isOpen, onClose, dayId, onSave} = this.props
        return (
            <Modal size={'lg'} isOpen={isOpen} toggle={onClose}>
                <ModalHeader toggle={onClose}>{dayId}</ModalHeader>
                <ModalBody>
                    <Hours itemArray={this.state.itemArray} onDblClick={this.createTimeRange} events={events}/>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={onSave}>Save</Button>
                    <Button color="secondary" onClick={onClose}>Cancel</Button>
                </ModalFooter>
            </Modal>
        )
    }

}

export default DayComponent


class Hours extends Component {

    creatingTime = () => {
        return localStorage.getItem('timeArray') ? JSON.parse(localStorage.getItem('timeArray')) : this.minutesCreate()
    }

    dateMaper = number => {
        switch(number){
            case 0: return '00';
            case 1: return '30';
            default: return null
        }
    }

    minutesCreate = () => {
        const result = [...new Array(48)].map((_, index) => {
            return `${Math.floor(index/2)}:${this.dateMaper(index%2)}`
        })
        localStorage.setItem('timeArray', JSON.stringify(result))
        return result
    }
    render() {
        const timeStamp = this.creatingTime()
        return (
            <div style={{
                display: 'flex',
                backgroundColor: 'rgb(250,250,250)',
            }}>
                <div style={{
                    width: '5%',
                }}>
                    {timeStamp.map((item, index)=> (
                        <div
                            id={`time-${item}`}
                            key={index}>
                            {item}
                        </div>
                    ))}
                </div>
                <div
                    onDoubleClick={e => {
                        this.props.onDblClick(e)
                    }}
                    style={{
                        width: '95%',
                        backgroundColor: 'rgb(230,230,230)'
                    }}>
                    {React.Children.map(this.props.itemArray, (item)=> {
                        return item
                    })}
                </div>

            </div>
        )
    }
}

