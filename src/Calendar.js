import React, {Component} from 'react'
import Month from './month/month'
import moment from 'moment'
import {Button, ButtonGroup} from 'reactstrap'
import DayComponent from "./dayComponent/dayComponent"

class Calendar extends Component {
    state = {
        date: localStorage.getItem('date') || new Date(),
    }

    openModal = (event, day, month, year) => {
        this.props.history.push('/' + day + '-' + month + '-' + year);
    }

    addDate = () => {
        const date = moment(this.state.date).add(1, 'month')
        this.setState({
            date,
        })
        localStorage.setItem('date', date)
    }

    onDayClick = (dayId) => {
        this.props.history.push(dayId)
    }

    onCloseModal = () => {
        this.props.history.push('/')
    }

    onSaveModal = () => {
        this.props.history.push('/')
    }

    subDate = () => {
        const date = moment(this.state.date).subtract(1, 'month')
        this.setState({
            date,
        })
        localStorage.setItem('date', date)
    }

    currenMonth = () => {
        const date = new Date()
        this.setState({
            date: date
        })
        localStorage.setItem('date', date)
    }


    render() {
        const {date} = this.state
        const {match} = this.props
        const {params} = match
        const isModalOpen = !!params.dayId
        return (
            <div>
                <div style={{
                    display: 'flex',
                    justifyContent: 'center'
                }}>
                    <ButtonGroup>
                        <Button color={'success'} onClick={this.subDate}>Previous month</Button>
                        <Button onClick={this.currenMonth}>CurrentMonth</Button>
                        <Button color={'success'} onClick={this.addDate}>Next month</Button>
                    </ButtonGroup>
                </div>
                <div style={{
                    display: 'flex',
                    justifyContent: 'center'
                }}>
                    <Month date={date} onDayClick={this.openModal}/>
                </div>
                {isModalOpen && <DayComponent
                    dayId={params.dayId}
                    isOpen={isModalOpen}
                    events={isModalOpen ? [1, 2, 3] : []}
                    onClose={this.onCloseModal}
                    onSave={this.onSaveModal}
                />}
            </div>

        )
    }
}

export default Calendar;
