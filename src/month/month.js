import React, {Component} from 'react'
import Cell from '../cell/cell'
import PropTypes from 'prop-types'
import moment from 'moment'
import './month.css'
import '../cell/cell.css'

class Month extends Component {
    state = {
        monthNumber: null,
        fullYear: null,
        daysCount: null,
    }

    static getDerivedStateFromProps({date}, state) {
        const m = moment(date)
        return {
            daysCount: m.daysInMonth(),
            monthNumber: m.format('MMMM'),
            fullYear: m.year(),
        }
    }

    onDayClick = (e, id) => {
        this.props.onDayClick(e, id, this.state.monthNumber, this.state.fullYear)
    }

    isCurrentFunc = (cell) => {
        const m = moment(new Date());
        return cell === m.date() && m.format('MMMM') === this.state.monthNumber && m.year() === this.state.fullYear
    }


    render() {
        const {monthNumber, fullYear, daysCount} = this.state
        const temp = moment(this.props.date).startOf('month').day() - 1
        const firstWeekday = temp === -1 ? 6 : temp
        const toSkip = new Array(firstWeekday).fill(-1)
        const range = new Array(daysCount).fill(null).map((_, index) => index)
        const rows = toSkip.concat(range).reduce((acc, item, indx) => {
            const rowIndx = Math.floor(indx / 7)
            if (!acc[rowIndx]) {
                acc[rowIndx] = []
            }
            acc[rowIndx].push(item)
            return acc
        }, [])
        return (
            <div>
                <span>
                    {monthNumber} {fullYear}
                </span>
                <div>
                    {rows.map((row, index) => (
                        <div
                            className={'weekInTheMonth'}
                            key={`${index}-row`}
                        >
                            {row.map((cell, indx) =>
                                <div
                                    key={`${index}-row-${indx}-day`}

                                >
                                    {cell >= 0 ?
                                        <Cell
                                            date={cell + 1}
                                            isWeekDay={!(indx !== 0 && (indx % 5 === 0 || indx % 6 === 0))}
                                            isCurrent={this.isCurrentFunc(cell + 1)}
                                            onDayClick={this.onDayClick}
                                            events={this.isCurrentFunc(cell + 1) ? ['Current  Day'] : []}
                                        /> :
                                        <div className={'cellSize'}/>}

                                </div>)}
                        </div>
                    ))}
                </div>
            </div>
        )
    }
}

Cell.propTypes = {
    date: PropTypes.number.isRequired,
}

export default Month